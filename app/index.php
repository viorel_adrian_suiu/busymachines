<?php
include "autoload.php";

use Facade\Facade;

$facade = new Facade();
$regres = $facade->displayRegresContent('https://reqres.in/api/users/2');
$goRest = $facade->displayGoRestContent('https://gorest.co.in/public-api/users');

echo '<div style="float: left"><span style="font-weight: bold">Regres API </span><br />'.$regres.'</div>';
echo '<div style="float: left"><span style="font-weight: bold">Gorest API</span> <br />'.$goRest.'</div>';
