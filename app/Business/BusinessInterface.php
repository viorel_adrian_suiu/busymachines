<?php
namespace Business;

interface BusinessInterface
{
    /**
     * @param string $api
     * @return mixed
     */
    public function displayContent(string $api);
}