<?php


namespace Business;

use AbstractClasses\AbstractBusiness;

class GoRest extends AbstractBusiness implements BusinessInterface
{

    /**
     * @param string $api
     * @return string
     */
    public function displayContent(string $api): string
    {
        $toReturn = '';
        $decodedContent = $this->decodeContent($api);
        foreach($decodedContent->data as $item)
        {
            $toReturn .= 'Id: '.$item->id.
                '<br /> Email'.$item->email.
                '<br /> Name: '.$item->name.
                '<br /> Gender: '.$item->gender.
                '<br /> Status: '.$item->status.
                '<br /> Created At: '.$item->created_at.
                '<br /> Updated At: '.$item->updated_at;
            $toReturn .= '<br />----<br />';
        }

        return $toReturn;
    }
}