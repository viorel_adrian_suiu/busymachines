<?php


namespace Business;


use AbstractClasses\AbstractBusiness;

class Regres extends AbstractBusiness implements BusinessInterface
{

    /**
     * @param string $api
     * @return string
     */
    public function displayContent(string $api): string
    {
        $toReturn = '';
        $decodedContent = $this->decodeContent($api);
        foreach($decodedContent as $item)
        {
            $toReturn .= 'Id: '.$item->id.
                '<br /> Email'.$item->email.
                '<br /> First name: '.$item->first_name.
                '<br /> Lastname: '.$item->last_name.
                '<br /> Avatar: '.$this->generateAvatar($item->avatar).
                '<br /> Url: '.$this->generateUrl($item->url).
                '<br /> Text: '.$item->text
            ;
        }

        return $toReturn;
    }

    /**
     * @param $img
     * @return string
     */
    private function generateAvatar($img): string
    {
        return '<img alt="" src="'.$img.'">';
    }

    /**
     * @param $url
     * @return string
     */
    private function generateUrl($url): string
    {
        return '<a href="'.$url.'">'.$url.'</a>';
    }
}