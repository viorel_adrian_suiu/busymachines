<?php


namespace AbstractClasses;


abstract class AbstractCurl
{
    /**
     * @param $url
     * @return mixed
     */
    protected function get($url): string
    {
        $cURLConnection = curl_init();

        curl_setopt($cURLConnection, CURLOPT_URL, $url);
        curl_setopt($cURLConnection, CURLOPT_RETURNTRANSFER, true);

        $result = curl_exec($cURLConnection);
        curl_close($cURLConnection);

        return $result;
    }
}