<?php

namespace AbstractClasses;

abstract class AbstractBusiness extends AbstractCurl
{
    /**
     * @param $api
     * @return false|string
     */
    protected function getApiContent($api)
    {
        return $this->get($api);
    }

    /**
     * @param $api
     * @return false|mixed|string
     */
    protected function getContent($api)
    {
        return $this->getApiContent($api);
    }

    /**
     * @param $api
     * @return mixed
     */
    protected function decodeContent($api)
    {
        return json_decode($this->getContent($api));
    }
}