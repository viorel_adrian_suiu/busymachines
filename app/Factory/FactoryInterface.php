<?php


namespace Factory;


use Business\GoRest;
use Business\Regres;

interface FactoryInterface
{
    /**
     * @return Regres
     */
    public static function createRegresObject(): Regres;

    /**
     * @return GoRest
     */
    public static function createGoRestObject(): GoRest;
}
