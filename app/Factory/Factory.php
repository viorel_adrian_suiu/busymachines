<?php
namespace Factory;

use Business\GoRest;
use Business\Regres;

class Factory implements FactoryInterface
{
    /**
     * @return Regres
     */
    public static function createRegresObject(): Regres
    {
        return new Regres();
    }

    /**
     * @return GoRest
     */
    public static function createGoRestObject(): GoRest
    {
        return new GoRest();
    }
}