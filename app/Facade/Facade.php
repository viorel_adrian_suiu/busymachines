<?php
namespace Facade;

use Factory\Factory;

class Facade implements FacadeInterface
{
    /**
     * @param string $api
     * @return string
     */
    public function displayRegresContent(string $api): string
    {
        return Factory::createRegresObject()->displayContent($api);
    }

    /**
     * @param string $api
     * @return string
     */
    public function displayGoRestContent(string $api): string
    {
        return Factory::createGoRestObject()->displayContent($api);
    }
}