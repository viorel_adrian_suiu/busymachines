<?php


namespace Facade;


interface FacadeInterface
{
    /**
     * @param string $api
     * @return string
     */
    public function displayRegresContent(string $api): string;

    /**
     * @param string $api
     * @return string
     */
    public function displayGoRestContent(string $api): string;
}